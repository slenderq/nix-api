FROM nixpkgs/nix:latest
# Copy our source and setup our working dir.
COPY . /app
WORKDIR /app

VOLUME ./nix /etc/nix

ARG NIX_PATH=nixpkgs=channel:nixos-unstable

CMD nix develop -c start

