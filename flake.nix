{
  description = "nix_api package managed with poetry2nix";

  inputs = {
    # nixpkgs = { url = "github:nixos/nixpkgs/nixpkgs-unstable"; };
    nixpkgs = { url = "github:nixos/nixpkgs/release-23.11"; };
    flake-utils = { url = "github:numtide/flake-utils"; };
    poetry2nix = {
      url = "github:nix-community/poetry2nix";
      inputs.nixpkgs.follows = "nixpkgs";
    };
  };

  outputs = { self, nixpkgs, flake-utils, poetry2nix, ... }:
    flake-utils.lib.eachDefaultSystem (system:
      let
        pkgs = import nixpkgs { inherit system; };
        inherit (poetry2nix.lib.mkPoetry2Nix { inherit pkgs; }) mkPoetryApplication mkPoetryEnv defaultPoetryOverrides;
        python = pkgs.python311Full;
        pythonPkgs = pkgs.python311Packages;


        pypkgs-build-requirements = {
          # ollama = [ "poetry" ];
          # icdiff = [ "setuptools" ];
          pprintpp = [ "setuptools" ];
        };
        poetryOverrides = defaultPoetryOverrides.extend (self: super:
          builtins.mapAttrs
            (package: build-requirements:
              (builtins.getAttr package super).overridePythonAttrs (old: {
                buildInputs = (old.buildInputs or [ ]) ++ (builtins.map (pkg: if builtins.isString pkg then builtins.getAttr pkg super else pkg) build-requirements);
              })
            )
            pypkgs-build-requirements
        );
        appEnv = mkPoetryEnv {
          projectDir = self;
          editablePackageSources = {
            nix-api = ./src;
          };
          overrides = poetryOverrides;
        };
        appPkgs = with pkgs; [
          poetry
          nixpkgs-fmt
        ];
        customScripts = [
          (pkgs.writeShellApplication {
            name = "validate";
            runtimeInputs = [ ];
            text = ''
              #!/usr/bin/env bash
              set -e
              nixpkgs-fmt .
              git add -u -p
              black .
              git add -u -p
            '';
          })
          (pkgs.writeShellApplication {
            name = "start";
            runtimeInputs = [ ];
            text = ''
              #!/usr/bin/env bash
              set -e
              uvicorn nix_api.main:app
            '';
          })
          (pkgs.writeShellApplication {
            name = "run_tests";
            runtimeInputs = [ ];
            text = ''
              #!/usr/bin/env bash
              # https://pypi.org/project/pytest-watch/
              set -e
              pytest --testmon || ptw -c
            '';
          })
          (pkgs.writeShellApplication {
            name = "build_test";
            runtimeInputs = [ ];
            text = ''
              #!/usr/bin/env bash
              nix develop -c run_tests
            '';
          })
          (pkgs.writeShellApplication {
            name = "rerun";
            runtimeInputs = [ ];
            text = ''
              #!/usr/bin/env bash
              # https://pypi.org/project/pytest-watch/
              set -e
              ptw -c
            '';
          })
          (pkgs.writeShellApplication {
            name = "build";
            runtimeInputs = [ ];
            text = ''
              #!/usr/bin/env bash
              set -e
              nix develop -c validate
              nix develop -c test
            '';
          })

        ];
      in
      rec {
        packages = {
          # https://ryantm.github.io/nixpkgs/languages-frameworks/python/#buildpythonpackage-function
          # https://github.com/nix-community/poetry2nix/blob/master/docs/edgecases.md
          nix-api = mkPoetryApplication {
            projectDir = self;
            overrides = poetryOverrides;
          };
          default = self.packages.${system}.nix-api;
        };

        # defaultPackage = packages.nix-api;
        devShells = {
          poetry = pkgs.mkShell {
            inputsFrom = [ self.packages.${system}.nix-api ];
            nativeBuildInputs = appPkgs ++ customScripts ++ [ appEnv ];
          };
        };

        devShell = devShells.poetry;

      });
}



