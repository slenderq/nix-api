# nix-api

This api is manage data of the following form:

|    Home ID |   Occupied |    Present |      State |
|-----------:|-----------:|-----------:|-----------:|
| abnt4bnd7m |       True |       True |       Home |
| xmrjtibxwt |      False |       True |      Sleep |
| 3swybccnhh |      False |      False |       Away |

## Features
- Fully reproducible hybrid development/deployment environment using Nix
- Thoroughly tested using `pytest` and various plugins 
- Python 3.11 type hinting and type checking via `pydantic` and `fastAPI`
- Separation of data model from API and database with `pydantic`
- Auto-generated API documentation/interface at the `/docs` endpoint via Swagger and FastAPI
- Code linting with `black`
- Support for non-NixOS operating systems through `docker-compose`
- Database bindings for future scalability or complete replacement with BigQuery
- Visually identifiable representation of Home IDs, as they are likely to be used by people as well as the program.

## Setup
### Start the Server
#### Running Locally on NixOS or with nix package manager (MacOS, Linux)
##### Requirements
Either [NixOS](https://nixos.org/) or the nix package manager installed.

To install the nix package manager run the following command:

```
sh <(curl -L https://nixos.org/nix/install) --daemon # Linux
sh <(curl -L https://nixos.org/nix/install) # MacOS
```

##### Enabling Experimental Features

If you get an error about experimental features. Use following command instead.
```
nix --extra-experimental-features 'nix-command flakes' develop -c start
```

##### Instructions

```
nix develop -c start
```
This will download and build the project and run a `uvicorn` server serving it.

#### Running via NixOS Docker Container and Docker Compose
##### Requirements
Docker and Docker compose installed on your system.

[Docker Install Documentation](https://docs.docker.com/get-docker/)

##### Instructions

```
docker-compose up -d # -d detatched so we keep control of our shell
```

This start a docker container and localhost:8000 on your network interface.

#### After the server is running...

View the documentation and start experimenting with the API!

[Documentation](http://localhost:8000/docs#/)

## Development

### Linting / Tests

```
nix develop -c validate # run linting
nix develop -c run_tests # run pytest tests
```
NOTE: Sometimes python code changes require a new `nix develop` shell to fully show up.

When in a `nix develop` shell:

```
build_test # rebuild the nix environment and run tests with new code changes 
```

### Adding Normal Linux Packages
In the `flake.nix` file, you can find the following variable:

```
appPkgs = with pkgs; [
      poetry
      nixpkgs-fmt
    ];
```

You can add any package from [NixPkgs](https://search.nixos.org/packages) to this list. Unlike the Python packages, these packages are very likely to work out of the box.

NixPkgs is very similar to the Arch User Repository (AUR), in that has everything and is very up to date. 


### Adding PyPi Packages via Poetry

```
nix develop # enter a nix develop shell 
poetry add <pkg> # update pyproject.yaml and poetry.lock
git commit -u -m "New poetry pkg <pkg>" # Flakes use the git tree
build # rebuild the nix develop shell to test the new package
```
If the build is successful your package should be available.

```
# test if the pkg was installed
python3
import <pkg>
```

##### It didn't build...
Many packages just plainly don't work out of the box with poetry2nix. Often requiring extra Nix to make it work. Often this is worth it but not an easy task.
https://github.com/nix-community/poetry2nix/blob/master/docs/edgecases.md


## Documentation

### Deployment Plan



```
docker-compose.yaml > Dockerfile > flake.nix > pyproject.toml > start
```

### Important Files/Directories

#### flake.nix 
A Nix expression that glues the project together.
In practice that means it manages:
- Installation of all packages, both from [NixPkgs](https://search.nixos.org/packages) and from PyPi via Poetry and [Poetry2Nix](https://github.com/nix-community/poetry2nix)

#### pyproject.toml
Using Poetry and Poetry2Nix rather than just Nix to manage and build python packages.
Also defines repo metadata and is a jumping off point to creating a pip package if that is ever needed.

#### Dockerfile
Simple Dockerfile that starts with NixOS, and runs `nix develop -c start`

Hopefully this file does not change very often and it contains very little application specific code.

#### docker-compose.yaml
Docker compose file to stand up the Docker container which conk


### File Structure
```
├── data # mounted data directory for docker
│   └── homes.db # shared SQLite data file
├── docker-compose.yaml # NixOS docker compose file to serve project
├── Dockerfile # NixOS base image docker file that runs nix develop -c start
├── flake.lock # Nix flake.lock, stores package paths, versions, etc.
├── flake.nix # Main nix configuration file for this project
├── __init__.py 
├── nix # mounted nix directory for NixOS specific container settings
│   └── nix.conf # enable container specific and experimental features
├── poetry.lock 
├── pyproject.toml # poetry project file. Mostly edited by the poetry command
├── README.md # the file you are reading now!
└── src # Application code
    └── nix_api # Python Package
        ├── main.py # FastAPI client setup and main for the API
        ├── model.py # Pydantic model for Home and other helpers to maintain state
        ├── routers # Allowing for more new endpoints in the future
        │   └── home.py # home endpoint API implementation via FastAPI
        ├── sql.py # Functions for interacting with the database. 
        └── tests # pytest tests
            ├── test_home.py
            ├── test_main.py
            ├── test_model.py
            └── test_sql.py
```


#### Database
The api has a sqlite3 database to track the state of homes. This is only for prototype purposes and I would want to connect this to BigQuery or someother big data solution so this can scale with ease.


#### My code does not seem to be updating!
Make sure to run `nix develop -c run_tests` when you make big changes. 

Often any change to module structure requires a rebuild of the environment.

### Wait, what's Nix?!? 

This guide is a very good intro with lots of different media about NixOS.

https://github.com/mikeroyal/NixOS-Guide


#### TL;DR
Nix allows for a highly reproducible and flexible development/deployment environment.

This enables an easy non-virtualized local batteries included developer experience that precisely matches the deployment environment using NixOS in Docker.

Developers on this repo do not need to know Nix and can use Poetry as expected.

## Scaling Plans
- Create more docker containers to horizontally scale the API. 
- Upgrade the database to something that is much more scalable (i.e. BigQuery)
- Create larger containers to vertically scale. Using FastAPI this should not be needed for a while.
- https://fastapi.tiangolo.com/benchmarks/

## Known problems
- The Dockerfile/Docker-compose needs more work than is in scope for this assignment. 
    - Currently we don't use docker layer/nix caching at all which means that our build is longer than it needs to be for a new version.
- Some areas don't have the required typing. These are areas where the `FastAPI` validation was causing more issues than worth my time.
    - In a real world case we would have time for full typing coverage and also another validation tool such as `mypy`


## Futher work
- Consider building our own OCI registery using the Nix snapshotter: https://github.com/pdtpartners/nix-snapshotter
- Upgrade the database to BigQuery or another suitable data solution
- Explore deploying tools like this one as a pip package using the existing poetry configuration for easy access by data science teams
- Develop a configuration.nix for NixOS containers to enable instant rollbacks and generations.
