"""Database adapor for the API"""

import sqlite3
from nix_api.model import Home
import time

# CONSIDERATION: Ideally this would be the adapor into a scalable platfrom other than sqlite, such a BigQuery. Mainly trying to show what the sql adapor structure would be like if this was more than a toy project.

db_path = "data/homes.db"


def migrate():
    """Runs when the migrate script is run. This usually contains any SQL that needs to be run for a given migration"""
    print("no migrations to run!")


def create_home_table():
    conn = sqlite3.connect(db_path)
    c = conn.cursor()
    c.execute(
        """CREATE TABLE IF NOT EXISTS homes (id TEXT PRIMARY KEY, occupied BOOLEAN, present BOOLEAN, state STRING)"""
    )
    conn.commit()
    conn.close()


def insert_home_in_db(home: Home):
    conn = sqlite3.connect(db_path)
    c = conn.cursor()
    c.execute(
        "INSERT INTO homes (id, occupied, present, state) VALUES (?, ?, ?, ?)",
        (home.id, home.occupied, home.present, home.state),
    )
    conn.commit()
    conn.close()


def update_home_in_db(home: Home) -> None:
    conn = sqlite3.connect(db_path)
    c = conn.cursor()
    c.execute(
        "UPDATE homes SET occupied = ?, present = ?, state = ? WHERE id = ?",
        (home.occupied, home.present, home.state, home.id),
    )
    conn.commit()
    conn.close()


def remove_home_in_db(home: Home) -> None:
    conn = sqlite3.connect(db_path)
    c = conn.cursor()
    c.execute("DELETE FROM homes WHERE id = ?", (home.id,))
    conn.commit()
    conn.close()


def get_home_by_id(id: str) -> Home | None:
    """Query the DB for a home, returning None if it does not exist"""
    conn = sqlite3.connect(db_path)
    c = conn.cursor()
    c.execute("SELECT id, occupied, present FROM homes WHERE id = ?", (id,))
    r = c.fetchone()
    conn.commit()
    conn.close()

    if not r:
        return None

    h = Home(id=r[0], occupied=r[1], present=r[2])

    return h


def get_all():
    conn = sqlite3.connect(db_path)
    c = conn.cursor()
    c.execute("SELECT id, occupied, present, state FROM homes")
    result = c.fetchall()

    homes = []
    for r in result:
        h = Home(id=r[0], occupied=r[1], present=r[2])
        homes.append(h)

    return homes


def store_home_in_db(home: Home):
    """If its a new home, add to the table, otherwise update the row"""
    result = get_home_by_id(home.id)
    if result:
        update_home_in_db(home)
    else:
        insert_home_in_db(home)
