"""API Router for the home endpoint"""

import logging

from fastapi import APIRouter, Depends, HTTPException
from fastapi.encoders import jsonable_encoder


from nix_api.model import Home
from nix_api.sql import (
    get_home_by_id,
    get_all,
    insert_home_in_db,
    update_home_in_db,
    remove_home_in_db,
)


class HomeNotFound(Exception): ...


logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)


# referenced as home.router in main.py
# This is the FastAPI recommendation to avoid namespace collisions.
router = APIRouter(
    prefix="/home",
    tags=["home"],
    responses={404: {"description": "Not found"}},
)


@router.get("/hello")
async def hello():
    """Basic endpoint mainly for smoke testing"""
    return "hi there!"


@router.get("/get/{home_id}")
async def get_home(home_id: str):
    """Retrieve a Home by its ID.

    Args:
        home_id (str): The unique identifier of the home.

    Returns:
        Home: The Home object retrieved.

    Raises:
        HomeNotFound: If the home with the specified ID is not found.
    """
    h = get_home_by_id(home_id)
    if h:
        return h
    raise HomeNotFound(f"Can't find home with id: {home_id}")


@router.post("/new", response_model=Home)
async def new(occupied: bool, present: bool):
    """creates a new home entry in the database.

    args:
        occupied (bool): indicates if the home is occupied.
        present (bool): indicates if someone is present in the home.

    returns:
        home: the newly created home entry.

    """
    h = Home(occupied=occupied, present=present)
    insert_home_in_db(h)
    logger.info(f"new home: {Home}")
    return h


@router.patch("/toggle_occupied/{home_id}", response_model=Home)
async def toggle_occupied(home_id: str):
    """Toggles the occupied status of a home by its ID.

    Args:
        home_id (str): The ID of the home to toggle.

    Returns:
        Home: The updated Home object after toggling the occupied status.
    """
    h = get_home_by_id(home_id)
    if not h:
        raise HomeNotFound
    # Flip the occupied flag
    print(h)
    h.occupied = not h.occupied
    print(h)
    update_home_in_db(h)
    return h


@router.patch("/toggle_present/{home_id}", response_model=Home)
async def toggle_present(home_id: str):
    """Toggles the present status of a home by its ID.

    Args:
        home_id (str): The ID of the home to toggle.

    Returns:
        Home: The updated Home object after toggling the present status.
    """
    h = get_home_by_id(home_id)
    if not h:
        raise HomeNotFound
    # Flip the occupied flag
    h.present = not h.present
    update_home_in_db(h)
    return h


@router.get("/get_all")
async def get_all_homes() -> list[Home]:
    """Return a list of all the homes in the database"""
    return jsonable_encoder(get_all())


@router.delete("/delete/{home_id}")
async def delete(home_id: str):
    """Deletes a home by its ID.

    Args:
        home_id (str): The ID of the home to be deleted.

    Raises:
        HomeNotFound: If the home with the specified ID is not found.

    Returns:
        dict: A dictionary indicating the success of the deletion operation.
    """
    h = get_home_by_id(home_id)
    if not h:
        raise HomeNotFound
    remove_home_in_db(h)
    return jsonable_encoder({"success": f"{home_id} deleted"})
