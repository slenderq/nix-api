"""FastAPI app"""

from fastapi import FastAPI
from fastapi.responses import JSONResponse

from .routers import home
from nix_api.sql import create_home_table, insert_home_in_db
from nix_api.model import DataError

description = """
Toy API example to test nixos api 
"""
tags_metadata = [
    {
        "name": "home",
        "description": "Home State API",
    },
]

create_home_table()

app = FastAPI(
    title="Nix api",
    description=description,
    openapi_tags=tags_metadata,
    version="v1",
)


# allow other endpoints to be created in the future.
app.include_router(home.router, prefix="/v1")


# Catch all server errors as 500
@app.exception_handler(Exception)
async def general_catch(request, exc):
    return JSONResponse(content={"internal_service_error": str(exc)}, status_code=500)


@app.exception_handler(home.HomeNotFound)
async def home_not_found(request, exc):
    return JSONResponse(content={"error": str(exc)}, status_code=404)


@app.exception_handler(DataError)
async def data_error(request, exc):
    return JSONResponse(content={"error": str(exc)}, status_code=400)


@app.get("/hello")
async def hello():
    return {"message": "Hello Ecobee!"}
