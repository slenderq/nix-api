"""Data model for the API in Pydantic

Example Homes:

|    Home ID |   Occupied |    Present |      State |
|-----------:|-----------:|-----------:|-----------:|
| abnt4bnd7m |       True |       True |       Home |
| xmrjtibxwt |      False |       True |      Sleep |
| 3swybccnhh |      False |      False |       Away |
"""

from random import choices

from pydantic import BaseModel, computed_field
from typing import Literal, Optional, Union


class DataError(Exception): ...


def generate_id(n=64) -> str:
    """Create a human-readable identifier n characters long

    Human-readable as the data is expected to be reviewed by individuals

    169,870,458,651,940,814,848 possible ids

    len(visual_chars) Choose N possible ids when n is 64
    """
    # CONSIDERATION: In a real-world scenario, a more formal identifier may be necessary.
    # In such cases, thorough research would be conducted to determine the most suitable identifier that can scale correctly for an increasing number of Home ids.
    # Reference: https://blog.devtrovert.com/p/how-to-generate-unique-ids-in-distributed
    visual_chars = [
        "a",
        "b",
        "c",
        "d",
        "e",
        "f",
        "h",
        "i",
        "j",
        "k",
        "m",
        "n",
        "o",
        "p",
        "r",
        "s",
        "t",
        "w",
        "x",
        "y",
        "3",
        "4",
        "7",
    ]
    return "".join(choices(visual_chars, k=n))


class Home(BaseModel):
    """Pydantic Model for a generic home"""

    id: str = "unset"  # uuid maybe or maybe the a visualually unique id?
    occupied: bool = False
    present: bool = False

    @computed_field
    @property
    def state(self) -> Literal["Home", "Away", "Sleep"]:
        """Get the state of home"""
        if self.occupied and not self.present:
            raise DataError(
                "Home is occupied but no one is present. This may be due to a sensor error."
            )

        if self.present:
            if self.occupied:
                return "Home"
            return "Sleep"

        return "Away"

    def __init__(self, occupied: bool, present: bool, id=None, **kwargs):
        super().__init__()
        if id == None:
            self.id = generate_id()
        else:
            self.id = id
        self.occupied = bool(occupied)
        self.present = bool(present)

    def __eq__(self, other):
        """Allow models to be compared via eq"""
        if self.id == other.id:
            if self.state == other.state:
                return True
        else:
            return False

    def __hash__(self):
        return hash(self.id) + hash(self.occupied) + hash(self.present)

    def __str__(self):
        return f"Home(ID:{self.id}, Occupided:{self.occupied}, Present:{self.present}, State:{self.state}"

    def __repr__(self):
        return f"Home(ID:{self.id}, Occupided:{self.occupied}, Present:{self.present}, State:{self.state}"


if __name__ == "__main__":
    h = Home(occupied=True, present=True)
    print(h)
