from time import sleep
import pytest
import os
import random
from fastapi.testclient import TestClient

from nix_api.main import app
from nix_api.sql import (
    create_home_table,
    store_home_in_db,
    get_home_by_id,
    get_all,
    db_path,
    remove_home_in_db,
)
from nix_api.model import Home

client = TestClient(app)


def test_hello():
    """Smoke test for fastAPI and the api routing"""
    response = client.get("/v1/home/hello")
    reponse_json = response.json()
    assert reponse_json == "hi there!"


def test_invalid_state_new():
    """Confirm that the API user gets a error when trying to create a Home with someone presenet but not occupied"""
    response = client.post(f"/v1/home/new?occupied=True&present=False")
    print(response.json())
    expected_error = {
        "error": "Home is occupied but no one is present. This may be due to a sensor error."
    }
    assert response.json() == expected_error


def test_toggle_occupation_to_invalid_state():
    """Test that we can't switch to an error state"""
    response = client.post(f"/v1/home/new?occupied=False&present=False")
    safe_h = Home(**response.json())

    response = client.patch(f"/v1/home/toggle_occupied/{safe_h.id}")
    print(response.json())
    expected_error = {
        "error": "Home is occupied but no one is present. This may be due to a sensor error."
    }
    assert response.json() == expected_error


def test_toggle_present_to_invalid_state():
    """Test that we can't switch to an error state"""
    response = client.post(f"/v1/home/new?occupied=True&present=True")
    print(response.json())
    safe_h = Home(**response.json())

    response = client.patch(f"/v1/home/toggle_present/{safe_h.id}")
    expected_error = {
        "error": "Home is occupied but no one is present. This may be due to a sensor error."
    }
    assert response.json() == expected_error


def test_get_not_found():
    """Test what happens when the id does not exist"""
    id = "notarealid"

    response = client.get(f"/v1/home/get/{id}")
    print(response.json())
    expected_error = {"error": "Can't find home with id: notarealid"}

    assert dict(response.json()) == expected_error


def test_get():
    """Test getting homes via /home/get"""
    old_h = Home(occupied=True, present=True)
    store_home_in_db(old_h)

    response = client.get(f"/v1/home/get/{old_h.id}")
    r = response.json()
    print(r)

    api_h = Home(**r)

    assert api_h == old_h


def test_new():
    """Test the /home/new endpoint"""
    response = client.post(f"/v1/home/new?occupied=True&present=True")
    r = response.json()
    print(r)

    new_h = Home(**r)
    assert new_h.id != "unset"
    assert type(new_h.occupied) == bool
    assert type(new_h.present) == bool
    print(new_h)

    response = client.get(f"/v1/home/get/{new_h.id}")
    expected_h = Home(**response.json())
    assert expected_h == new_h


def test_toggle_present():
    """Test toggling the present field"""
    h = Home(occupied=False, present=True)
    store_home_in_db(h)
    response = client.patch(f"/v1/home/toggle_present/{h.id}")

    edited_h = Home(**response.json())
    print(edited_h)

    response = client.get(f"/v1/home/get/{edited_h.id}")
    expected_h = Home(**response.json())

    assert edited_h == expected_h


def test_toggle_occupated():
    """Test toggling the occupated field"""
    old_h = Home(occupied=False, present=True)
    assert old_h.id != "unset"
    store_home_in_db(old_h)
    client.patch(f"/v1/home/toggle_occupated/{old_h.id}")

    sleep(1)

    response = client.get(f"/v1/home/get/{old_h.id}")
    expected_h = Home(**response.json())

    print(expected_h)

    assert not expected_h.occupied


def test_home_delete():
    response = client.post(f"/v1/home/new?occupied=True&present=True")
    to_delete_r = Home(**response.json())

    delete_r = client.delete(f"/v1/home/delete/{to_delete_r.id}")

    assert {"success": f"{to_delete_r.id} deleted"} == delete_r.json()

    # check if the new home has been removed
    response = client.get(f"/v1/home/get/{to_delete_r.id}")
    expected_error = {"error": f"Can't find home with id: {to_delete_r.id}"}
    assert dict(response.json()) == expected_error
