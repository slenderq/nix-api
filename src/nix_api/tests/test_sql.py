import os
import pytest
import random

from nix_api.sql import (
    create_home_table,
    store_home_in_db,
    get_home_by_id,
    get_all,
    db_path,
    remove_home_in_db,
)
from nix_api.model import Home, DataError


@pytest.fixture(autouse=True)
def db():
    """Testing fixture that serves a fresh database to every test"""
    os.remove(db_path)
    create_home_table()  # incase we haven't


def test_get_home_simple():
    """Smoke test for basic db operations"""
    h = Home(occupied=True, present=True)
    store_home_in_db(h)

    db_h = get_home_by_id(h.id)

    assert h == db_h


def test_insert_vs_update():
    """Check that we do indeed update if the id is the same"""
    h = Home(occupied=True, present=True)
    assert h.state == "Home"
    store_home_in_db(h)

    h.occupied = False

    # insert the same record.
    store_home_in_db(h)

    db_h = get_home_by_id(h.id)

    assert h == db_h
    assert db_h.state == "Sleep"


def test_push_invalid():
    """Push a occuipied home with nonone present"""
    h = Home(occupied=True, present=True)
    store_home_in_db(h)

    h.present = False

    with pytest.raises(DataError):
        store_home_in_db(h)


def test_get_all():
    homes = []
    for i in range(1, 100):
        try:
            h = Home(
                occupied=random.choice([True, False]),
                present=random.choice([True, False]),
            )
            store_home_in_db(h)
            homes.append(h)
        except DataError:
            continue

    db_homes = get_all()
    print(db_homes)

    assert len(set(homes) - set(db_homes)) == 0


def test_remove_home():
    h = Home(occupied=True, present=True)
    store_home_in_db(h)
    remove_home_in_db(h)
    assert get_home_by_id(h.id) == None
