from fastapi.testclient import TestClient

from nix_api.main import app

client = TestClient(app)


def test_hello():
    response = client.get("/hello")
    reponse_json = response.json()
    assert reponse_json == {"message": "Hello Ecobee!"}
