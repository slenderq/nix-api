import pytest
from nix_api.model import Home, DataError


def test_model_create():
    """Basic Model Create Smoke Test"""
    h = Home(occupied=True, present=True)
    print(h)
    assert h.id != "unset"


def test_correct_serialization():
    h = Home(id="custom_id", occupied=True, present=True)
    assert type(h.occupied) == bool
    assert type(h.present) == bool


def test_all_states():
    """Confirm that the states are changed as expected"""
    h = Home(occupied=True, present=True)
    assert h.state == "Home"

    h.occupied = False
    assert h.state == "Sleep"

    h.present = False
    assert h.state == "Away"

    h.occupied = True
    with pytest.raises(DataError):
        _ = h.state
